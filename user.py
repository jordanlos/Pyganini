import tdl
from playhead import Playhead_Node, Playhead
from tab import Fret, Tab
import mus
import sheetmusic
from threading import Thread

#############################################
#       Helper Functions                    #
#############################################
# Constrains cursor within y (x constrained by bars)
def constrain(n, lower_bound, upper_bound):
    if n < lower_bound:
        return lower_bound
    elif n > upper_bound:
        return upper_bound
    else:
        return n


#############################################
# User Input Functions                      #
#############################################
class User():
    def __init__(self, tab):
        self.next_fret = ''  # Builds Frets
        self.tab = tab  # Instance of Fret Class
        self.playhead = None  # Instance of Playhead
        self.player = None  # Instance of Player

    def add_player(self, player):
        self.player = player

    def add_playhead(self, playhead):
        self.playhead = playhead

    def build_fret(self, incoming):
        '''
        String Builder Function for building multi-character fret numbers
        '''
        fret_number = incoming.char

        # Validate Input
        is_fret = False
        for i in range(10):
            if fret_number == str(i):
                is_fret = True

        # Update the Tab to reflect input
        if is_fret:
            return fret_number
        else:
            return ''

    def move_cursor(self, keystroke):
        '''
        Reads a keystroke and adjusts the position of the cursor on screen
        and the position playhead.
        '''
        if keystroke == 'UP':
            old_string = self.playhead.get_string()
            new_string = constrain(old_string - 1, 0, 5)
            self.playhead.set_string(new_string)
            self.player.move(0, -1)

        elif keystroke == 'DOWN':
            old_string = self.playhead.get_string()
            new_string = constrain(old_string + 1, 0, 5)
            self.playhead.set_string(new_string)
            self.player.move(0, 1)

        elif keystroke == 'LEFT':
            jump_bar = 0
            if self.playhead.get_score_position() % 4 == 0:
                jump_bar = -1  # Move extra position due to barline
            self.playhead.move_left()
            self.player.move(-3 + jump_bar, 0)
            print(self.playhead.get_score_position(), end=' ')
            print(self.playhead.get_node().get_note_length())

        elif keystroke == 'RIGHT':
            self.playhead.move_right()
            jump_bar = 0
            pos = self.playhead.get_score_position()
            if pos % 4 == 0 and pos != 0:
                jump_bar = 1
            self.tab.update_bar(self.playhead, self.player.x)
            self.player.move(3 + jump_bar, 0)
            print(self.playhead.get_score_position(), end=' ')
            print(self.playhead.get_node().get_note_length())

    def handle_keys(self):
        '''
        Processes input keys to make changes to the tab on screen or data
        '''
        keypress = False
        events = tdl.event.get()
        is_move_key = ('UP', 'DOWN', 'LEFT', 'RIGHT')
        user_input = str()

        # The tdl library captures user input events.
        # This loop first catches fret character inputs.
        # Failing any frets to input, runs other user input functions.
        for event in events:
            if event.type == 'KEYDOWN':
                keypress = True
                user_input = event
                # Deletes an existing fret
                if event.char == '-':
                    self.playhead.update_note(None)
                    self.tab.enter_note('-', self.player.x, self.playhead.get_string())
                self.next_fret += self.build_fret(event)
                # Fret numbers can be 2 digits, so the program needs to distinguish from individual and multi-digit fret inputs
                if len(self.next_fret) >= 1:
                    if len(self.next_fret) > 1 or event.key in is_move_key or event.key == 'ENTER':
                        self.playhead.update_note(self.next_fret)
                        self.tab.enter_note(self.next_fret, self.player.x, self.playhead.get_string())
                        print(self.next_fret)
                        self.next_fret = ''
                        # Check to move cursor

        # Do nothing if there no keypress
        if not keypress:
            return
        # Inserts a New Note
        elif user_input.key == 'F1':
            self.playhead.insert_node(Playhead_Node(self.playhead.playhead_length))
            self.playhead.move_right()
            jump_bar = 0
            pos = self.playhead.get_score_position()
            if pos % 4 == 0 and pos != 0:
                jump_bar = 1
            self.tab.update_bar(self.playhead, self.player.x)
            self.player.move(3 + jump_bar, 0)
        # Makes the length of the current note a 1/4 note (1 beat)
        elif user_input.key == 'F3':
            self.playhead.update_note_length(1)
            self.tab.update_bar(self.playhead, self.player.x - 3)
        # Makes the length of the current ntoe an 1/8 note (.5 beat)
        elif user_input.key == 'F4':
            self.playhead.update_note_length(0.5)
            self.tab.update_bar(self.playhead, self.player.x - 3)
        # Makes the length of the current ntoe an 1/16 note (.25 beat)
        elif user_input.key == 'F5':
            self.playhead.update_note_length(0.25)
            self.tab.update_bar(self.playhead, self.player.x - 3)
        # Saves tab to file
        elif user_input.key == 'F6':
            self.fretboard_to_file()
        # Decreases tempo
        elif user_input.key == 'F7':
            self.playhead.change_tempo(-5)
        # Increases tempo
        elif user_input.key == 'F8':
            self.playhead.change_tempo(5)
        # Quits the Program
        elif user_input.key == 'ESCAPE':
            # fretboard_to_file(self.self.tab,playhead)
            return True  # exit game
        # Moves the cursor
        self.move_cursor(user_input.key)

    def fretboard_to_file(self):
        '''
        Saves the current program as a .text and .pyg file

        TEXT FILE:
        The text file looks nearly identical to the screen and resembles the for tab format common
        online, with the improved addition of note lengths. The text format makes the guitar tab easy
        to share while keeping formatting consistent.

        PYG
        A special file designed specifically for loading a tab into Pyganini.
        The file format stores 1 line/playhead_node where each line
        holds 6 notes and the duration of the note length.
        '''
        filename = self.tab.get_name()
        with open(filename + '.text', 'w') as f:
            fretboard = self.tab.get_fretboard()
            for row in range(self.tab.get_height()):
                for col in range(self.tab.get_width()):
                    fret = fretboard[col][row]
                    f.write(fret.char)
                f.write('\n')

        with open(filename + '.pyg', 'w') as g:
            node = self.playhead.get_head()
            notes = list()
            score_position = 0
            j = 0
            while node is not None:
                write_str = ''
                for i in node.notes:
                    if i is not None:
                        write_str += str(i) + ','
                    else:
                        write_str += '-' + ','
                write_str += ' '
                write_str += str(node.get_note_length())
                g.write(write_str)
                g.write('\n')
                node = node.get_next()
                j += 1
