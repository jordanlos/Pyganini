"""
Dependencies:
    lilypond - run lilypond-2.18.2-1.linux-64.sh in /Libraries/
Notes:
    lilypond is software that develops sheet music
"""
from subprocess import call


def get_duration(durNum):
    # converts our duration format to lilypond's
    if durNum == 0.25:
        return '16'  # 16th note
    elif durNum == 0.5:
        return '8'  # 8th note
    elif durNum == 1:
        return '4'  # quarter note
    elif durNum == 4:
        return '1'  # full note
    else:
        return '2'  # half note and catch for incorrect duration


def get_note(number):
    notelist = ["c", "cs", "d", "ds", "e", "f", "fs", "g", "gs", "a", "as", "b"]
    return notelist[number]

def get_octave(number):
    if number > 48: # if it's less than this, no octave indicator needed
        string = ""
        # floor division gets number of octaves above second octave (C2)
        for i in range((number - 36) // 12):
            string += "'"  # adds lilypond ocatve notation to string
        return string
    else:
        return ""


def get_nORch(full_note):
    chordlist = list()
    for i in range(6):
        if full_note[0][i] is not None:
            # mod 12 assuming C is 0 and B is 11
            note = get_note(int(full_note[0][i]) % 12)
            octave = get_octave(int(full_note[0][i]))
            if i != 5:
                chordlist.append(note + octave)
            else:
                chordlist.append(note + octave)

    if chordlist == []:
        return "r"
    elif len(chordlist) == 1:
        return chordlist[0]
    else:
        return "<" + " ".join(chordlist) + ">"


def get_totalNote(full_note):
    # 'note' should be part of the note list

    note = get_nORch(full_note)
    duration = get_duration(float(full_note[1]))  # convert duration of note
    return note + duration  # spit out full lilypond note


def createsheet(filename, notes, time='4/4', title=None, key='c \major'):
    """
    filename and notes(list) is required
    time signature is default 4/4
    title will default use the filename before the extension
    key is default c major
    """

    # assumes filename is 'file.extension'
    fileWOext = filename.split('.')[0]
    if title is None:
        title = fileWOext

    with open(filename, 'w') as f:
        f.write('\\version "2.18.2-1"\n')  # version statement
        f.write('\language "english"\n')  # allows us to use english half keys
        f.write('\header {\n' + 'title = "' + title + '"\n' + '}\n')
        f.write('\\time ' + time + '\n')
        # f.write('\\tempo 4 = ' + tempo + '\n')
        f.write('{\n')
        f.write('\key ' + key + '\n')
        note_list = list()
        for i in range(len(notes)):
            note_list.append(get_totalNote(notes[i]))
        f.write(" ".join(note_list))
        f.write('\n}')

    call(["lilypond", filename])
    call(["xdg-open", fileWOext + '.pdf'])
