#!/usr/bin/env python3
# README
############################################
#          TODO's                    #
############################################
#Need to finish
#TODO: Legend on screen
#TODO: Screen follows cursor
#TODO: Avoid emtpy notes
#TODO: Prevent Uneven bar Lenghts
#TODO: First note only works if 1/4 note
#TODO: Move between notes left and right
#TODO: Delete Notes

#Stylistic/ User stuff
#TODO: Add option to save name
#TODO: Add legend
#TODO: Add Cool Title

#Strech Goals
#TODO: Enter notes without hitting enter (just type and left/right/up/down cancles
#TODO: Tie notes
#TODO: Note Groupings (3rd, 5ths, etc)
############################################
# Libraries and Global Parameters           #
#############################################q
# Main Library for game
import tdl

# Screen Parameters
SCREEN_WIDTH = 80
SCREEN_HEIGHT = 40

# Map Parameters
# For now only 6 lines high, one for each line
MAP_WIDTH = 120
MAP_HEIGHT = 8
BACKGROUND_COLOR = (0, 0, 0)
CHAR_COLOR = (255,255,255)
LIMIT_FPS = 40

# MUSIC PARAMETERS
TIME_SIGNATURE = 4

#############################################
# Misc. Helper Functions                    #
#############################################
# Constrains cursor within y (x constrained by bars)
def constrain_y(n):
    if n < 0:
        return 0
    elif n > MAP_HEIGHT - 1:
        return MAP_HEIGHT - 1
    else:
        return n

# Adds a straight bar indicating bar length is done
def add_char_line(column,char):
    for i in range(6):
        fretboard[column][i].char = char


# Adds the extra dashes and bar staffs where necessary
def update_bar(cursor_x):
    # TODO: First if for first note only
        if note_settings.value == 4:
            add_char_line(cursor_x + 2,'-')
            add_char_line(cursor_x + 3,'-') 
            add_char_line(cursor_x + 1,'-')
            add_char_line(cursor_x + 4,'|')
            add_char_line(cursor_x + 5,'|')
            player.move(2,0)
        elif  note_settings.value % note_settings.bar_length == 0:
            # line after note
            add_char_line(cursor_x + 1,'-')
            # Bar line
            add_char_line(cursor_x + 2,'|')
            # First new bar line - will be where cursor is
            add_char_line(cursor_x + 3,'-')
            # Position after cursor
            add_char_line(cursor_x + 4,'-')
            # move 2 normal spaces plus extra for bar line
            add_char_line(cursor_x + 5,'|')
            add_char_line(cursor_x + 6,'|')
            player.move(3,0)
        else:
            add_char_line(cursor_x + 2,'-')
            add_char_line(cursor_x + 3,'-') 
            add_char_line(cursor_x + 4,'|')
            add_char_line(cursor_x + 5,'|')
            player.move(2,0)

def add_staff(cursor_x):
    # TODO: Have to mix 16th/quater
    fretboard[cursor_x][6].char = '|'
    fretboard[cursor_x][7].char = '|'

    nl = note_settings.note_length
    note_settings.sub_div_grouping += nl
    # Add note groupings
    if note_settings.sub_div_grouping % 4 == 0:
        pass
    elif nl == 2:
        fretboard[cursor_x + 1][7].char = '_'
    elif nl == 1:
        fretboard[cursor_x + 1][6].char = '_'
        fretboard[cursor_x + 1][7].char = '_'




# Updates the cursor and tab to allow for fret numbers > 9
def add_second_digit(cursor_x):
    add_char_line(cursor_x + 2,'-')
    add_char_line(cursor_x + 3,'-')
    add_staff(cursor_x)
    player.move(1,0)

def enter_note(cursor_x):
    note_settings.value += note_settings.note_length
    if note_settings.entered_digits == 0:
        add_staff(cursor_x)
    else:
        note_settings.entered_digits = 0
#############################################
# Classes                                   #
#############################################
# Global Variables
class Fret:
    #a tile of the map and its properties
    def __init__(self, char = ' ', duration = 0):
        self.char = char
        self.duration = duration


class TabObject:
    # Generic object that can be anything
    # Its always a character on screen
    def __init__(self, x, y, char, color = CHAR_COLOR):
        self.x = x
        self.y = y
        self.char = char
        self.color = color

    def move(self, dx, dy):
        # move by specified amount
        # y needs to be strained only. X constrained by barlines
        y = constrain_y(self.y + dy)
        self.x += dx
        self.y = y

    def draw(self):
        # comment
        con.draw_char(self.x, self.y, self.char, self.color)

    def clear(self):
        # comment
        con.draw_char(self.x, self.y, ' ', self.color, bg=None)

class Note_Settings:
    ''' Class that tracks how many notes to determine when to add bars.
    Global variables can introduce bugs due to namespaces and scoping.
    Therfore, a class is safer
    '''
    # number of notes in bar so far
    value = 0
    # Bar length assumes 16th notes
    bar_length = 16
    # For entering multi-digit fret numbers
    entered_digits = 0
    # length of current note
    # Defaults to 4 (i.e. 4 16th notes or 1 beat)
    note_length = 4
    # For note Groupings
    sub_div_grouping = 0

#############################################
# User Input Functions                      #
#############################################
def move_cursor(keystroke):
    if keystroke == 'UP':
        player.move(0, -1)

    elif keystroke == 'DOWN':
        player.move(0, 1)

    '''
    TODO: These should move between notes only
    TODO: Need to be able to delete notes first
    elif keystroke == 'LEFT':
        player.move(-1, 0)

    elif keystroke == 'RIGHT':
        player.move(1, 0)
    '''

def enter_fret(incoming):
    # Initialize Variables
    x = player.x
    y = player.y
    fret_number = incoming.char
    duration = note_settings.note_length

    # Validate Input
    is_fret = False
    if fret_number == '-':
        is_fret = True
    for i in range(24):
        if fret_number == str(i):
            is_fret = True

    # Update the Tab to reflect input
    if is_fret:
        # Adds a new note, and appends it to the objects
        new_note = TabObject(x, y, fret_number)
        objects.append(new_note)

        # Prints it to the fretboard
        fretboard[x][y].char = fret_number
        fretboard[x][y].duration = duration

    if incoming.key == 'RIGHT' and note_settings.entered_digits == 0:
        note_settings.entered_digits = 1
        add_second_digit(x)

    if incoming.key == 'ENTER':
        # Updates the number of notes
        enter_note(x)
        update_bar(x)


def handle_keys():
    # Get keystroke
    keypress = False
    entering_fret = False
    events = tdl.event.get()
    user_input = str()
    for event in events:
        if event.type == 'KEYDOWN':
            user_input = event
            keypress = True
            enter_fret(user_input)

    if not keypress:
        return

    # Modify Note Lengths
    # TODO Could this be moved elsewhere?
    elif user_input.key == 'TAB':
        note_settings.note_length = 4
    elif user_input.key == 'CONTROL':
        note_settings.note_length = 2
    elif user_input.key == 'ALT':
        note_settings.note_length = 1
    elif user_input.key == 'ESCAPE':
        return True  #exit game

    # Check to move cursor
    move_cursor(user_input.key)


#############################################
# GUI Processes                             #
#############################################


def make_fretboard():
    global fretboard
    # Map[col][row]. MAP_WIDTH = # columns; MAP_HEIGHT = # rows
    fretboard = [[ Fret() for y in range(MAP_HEIGHT) ] for y in range(MAP_WIDTH)]
    # First Bar Line
    add_char_line(0,'|')
    add_char_line(1,'|')
    add_char_line(2,'-')
    # Last Bar Line
    add_char_line(3,'|')
    add_char_line(4,'|')


def add_char_to_fretboard(col, row, char):
    con.draw_char(col, row, char, fg=CHAR_COLOR, bg=BACKGROUND_COLOR)


def render_all():
    for col in range(MAP_WIDTH):
        for row in (range(MAP_HEIGHT)):
            fret = fretboard[col][row]
            add_char_to_fretboard(col, row, fret.char)
    for obj in objects:
        obj.draw()

    # Blits con with root at top left of screen
    # Needs to happen to update scren
    root.blit(con, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0)


#############################################
# I/O                                       #
#############################################
def fretboard_to_file():
    with open('savemap.text','w') as f:
        for row in range(MAP_HEIGHT):
            for col in range(MAP_WIDTH):
                fret = fretboard[col][row]
                f.write(fret.char)
            f.write('\n')


#############################################
# Initialization & Main Loop                #
#############################################
note_settings = Note_Settings()

tdl.set_font('terminal10x16_gs_tc.png', greyscale=False, altLayout=True)
root = tdl.init(SCREEN_WIDTH, SCREEN_HEIGHT, title="Roguelike", fullscreen=False)
tdl.setFPS(LIMIT_FPS)
con = tdl.Console(SCREEN_WIDTH, SCREEN_HEIGHT)

# Player
player = TabObject(2, 0, '@', CHAR_COLOR)

# All game objects
objects = [player]

# Generate map (not drawn to screen yet)
make_fretboard()

while not tdl.event.is_window_closed():
    # Draw all Objects
    render_all()

    tdl.flush()

    # Clear all old positions
    for obj in objects:
        obj.clear()

    #handle keys and exit game if needed
    exit_game = handle_keys()
    if exit_game:
        break
notes = list()

# Note Graph (Start-end, duration, note)
# Bottom E pitch 40
number_of_notes = 0
notes = list()
# At each fret object, have a note, and duration
playhead = 0 # Indicates point in song
for i,col in enumerate(fretboard):
    if col[6].char == '|':
        duration = 0
        fret = int()
        for i in range(5,-1,-1):
            if col[i].char != '-':
                duration = col[i].duration
                fret = int(col[i].char)
                break
        notes.append((playhead, duration,fret))
        playhead += duration

print(notes)
# This is a fole
# asdf
print()


# Prints tab to .text file
fretboard_to_file()
