#!/usr/bin/env python3
############################################
#              Contents                    #
#  0. Libraries and Global Parameters      #
#  1. I/O                                  #
#  2. GUI Objects                          #
#  3. TDL OBjects                          #
#  4. GUI Rendering Functions              #
#  5. Initialization and Main Loop         #
#  6. Post-Processing of Music             #
#                                          #
#                                          #
#                                          #
############################################

############################################
# Libraries and Global Parameters          #
############################################
# Main Library for game
import tdl  # Main Graphic Library
from collections import Counter
# Music Processings Modules
import mus
import textwrap
import shutil
from sheetmusic import createsheet
# OOP architecture modules
from user import User, constrain
from playhead import Playhead_Node, Playhead
from tab import Fret, Tab
tab = Tab()
user = User(tab)

# GUI Visual Parameters
SCREEN_WIDTH = 100
SCREEN_HEIGHT = 40
BAR_WIDTH = 20
PANEL_HEIGHT = 7
PANEL_Y = SCREEN_HEIGHT - PANEL_HEIGHT

#############################################
# I/O                                       #
#############################################
def file_to_fretboard(filename):
    '''
    Reads from a .pyg file and converts the file to a
    linked list of playhead_notes and points to the first note

    Returns a single (empty) playhead if no file found
    '''
    fretboard = tab.init_fretboard()
    playhead = Playhead()
    try:
        with open(filename +'.pyg', 'r') as f:
            line = f.readline()
            while line:
                notes, duration = line.split()
                notes = notes.split(',')[:-1] # Last index empty
                node = Playhead_Node(float(duration))
                for i,note in enumerate(notes):
                    if note == '-':
                        node.set_note(i,None)
                    else:
                        node.set_note(i,str(note))
                playhead.insert_node(node)
                playhead.set_node(playhead.get_node().get_next())
                line = f.readline()
            playhead.set_node(playhead.get_head())
            return playhead
    except:
            return playhead


#############################################
# GUI Objects                               #
#############################################
class TabObject:
    # Generic object that can be anything
    # Its always a character on screen
    def __init__(self, x, y, char, color = (255,255,255)):
        self.x = x
        self.y = y
        self.char = char
        self.color = color

    def move(self, dx, dy):
        # move by specified amount
    # y needs to be strained only. X constrained by barlines
        x_max = 2 + playhead.get_length() + playhead.get_score_position() // 4
        self.y = constrain(self.y + dy, 10, 15)
        self.x = constrain(self.x + dx, 2, 10000)

    def draw(self):
        # comment
        con.draw_char(self.x, self.y, self.char, self.color)

    def clear(self):
        # comment
        con.draw_char(self.x, self.y, ' ', self.color, bg=None)

def add_char_to_fretboard(col, row, char):
    con.draw_char(col, row, char, fg=tab.char_color, bg=tab.background_color)

# Player
player = TabObject(2, 10, '@', tab.char_color)


#############################################
# TDL Objects and setting                   #
#############################################
tdl.set_font('terminal10x16_gs_tc.png', greyscale=False, altLayout=True)
tdl.setFPS(tab.limit_fps)
# Consoles
root = tdl.init(SCREEN_WIDTH, SCREEN_HEIGHT, title="Pyganini", fullscreen=False)
con = tdl.Console(tab.get_width(), SCREEN_HEIGHT)
panel = tdl.Console(SCREEN_WIDTH, PANEL_HEIGHT)
title = tdl.Console(SCREEN_WIDTH, 5)


#############################################
# GUI Rendering Functions                   #
#############################################

def render_bar(panel, x, y, total_width, text, bar_color, back_color):
    #render the background first
    panel.draw_rect(x, y, total_width, 1, None, bg=back_color)

    #finally, some centered text with the values
    x_centered = x + (total_width-len(text))//2
    panel.draw_str(x_centered, y, text, fg=(255,255,255), bg=None)

def render_all():
    fretboard = tab.get_fretboard()
    for col in range(tab.get_width()):
        for row in (range(tab.get_height())):
            fret = fretboard[col][row]
            add_char_to_fretboard(col, row + 10, fret.char)
    player.draw()
    root.blit(con, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0)
    # Title
    title.clear(fg=(255,255,255), bg=(0,0,0))
    render_bar(title, 30, 0, 35, '', (255,255,255), (0,0,0))
    render_bar(title, 30, 1, 35, filename.upper(), (255,255,255), (0,00,0))
    render_bar(title, 30, 2, 35, '', (255,255,255), (0,0,0))
    root.blit(title, 0, 0, SCREEN_WIDTH, 5, 0, 0)    # Blits con with root at top left of screen

    # Display Panel at bottom
    panel.clear(fg=(255,255,255), bg=(0,0,0))
    render_bar(panel, 10, 0, 40, 'F1 = Insert', (100,255,100), (255,100,255))
    render_bar(panel, 10, 1, 40, 'F3 = 1/4', (100,255,100), (255,100,255))
    render_bar(panel, 10, 2, 40, 'F4 = 1/8', (100,255,100), (255,100,255))
    render_bar(panel, 10, 3, 40, 'F5 = 1/16', (100,255,100), (255,100,255))
    render_bar(panel, 10, 4, 40, 'F6 = Save Score', (100,255,100), (255,100,255))
    render_bar(panel, 10, 5, 40, 'Esc = Quit', (100,255,100), (255,100,255))
    render_bar(panel, 10, 6, 40, "'-' = Remove fret ", (100,255,100), (255,100,255))
    render_bar(panel, 50, 0, 40, 'F7/F8 -/+ tempo', (100,255,100), (255,100,255))
    render_bar(panel, 50, 1, 40, 'Tempo: ' + str(playhead.get_tempo()), (100,255,100), (255,100,255))
    render_bar(panel, 50, 2, 40, '', (100,255,100), (255,100,255))
    render_bar(panel, 50, 3, 40, 'Bars dont look right?', (100,255,100), (255,100,255))
    render_bar(panel, 50, 4, 40, 'Their length must = 4', (100,255,100), (255,100,255))
    render_bar(panel, 50, 5, 40, '', (100,255,100), (255,100,255))
    render_bar(panel, 50, 6, 40, '', (100,255,100), (255,100,255))

    #blit the contents of "panel" to the root console
    root.blit(panel, 0, PANEL_Y, SCREEN_WIDTH, PANEL_HEIGHT, 0, 0)    # Blits con with root at top left of screen


def menu(header, options, width):
    #calculate total height for the header (after textwrap) and one line per option
    header_wrapped = textwrap.wrap(header, width)
    header_height = len(header_wrapped)
    if header == '':
        header_height = 0
    height = len(options) + header_height

    #create an off-screen console that represents the menu's window
    window = tdl.Console(width, height)

    #print the header, with wrapped text
    window.draw_rect(0, 0, width, height, None, fg=(255,255,255), bg=None)
    for i, line in enumerate(header_wrapped):
        window.draw_str(0, 0+i, header_wrapped[i])

    #print all the options
    y = header_height
    letter_index = ord('a')
    for option_text in options:
        text = '(' + chr(letter_index) + ') ' + option_text
        window.draw_str(0, y, text, bg=None)
        y += 1
        letter_index += 1

    #blit the contents of "window" to the root console
    x = SCREEN_WIDTH//2 - width//2
    y = SCREEN_HEIGHT//2 - height//2
    root.blit(window, x, y, width, height, 0, 0, fg_alpha=1.0, bg_alpha=0.7)

    #present the root console to the player and wait for a key-press
    tdl.flush()

    key = tdl.event.key_wait()
    key_char = key.char
    if key_char == '':
        key_char = ' ' # placeholder

    if key.key == 'ENTER' and key.alt:
        #Alt+Enter: toggle fullscreen
        tdl.set_fullscreen(not tdl.get_fullscreen())

    #convert the ASCII code to an index; if it corresponds to an option, return it
    index = ord(key_char) - ord('a')
    if index >= 0 and index < len(options):
        return index
    return None


def main_menu():
    while not tdl.event.is_window_closed():
        #show the background image, at twice the regular console resolution
        #show options and wait for the player's choice
        title = 'Pyganini Guitar Editor'
        center = (SCREEN_WIDTH - len(title)) // 2
        root.draw_str(center - 1, SCREEN_HEIGHT//2-4, title, bg=None, fg=(50,200,50))

        choice = menu('', ['Name A New Tab', 'Name Existing Tab','Demo 1','Demo 2'], 24)

        if choice == 0:  # New Tab
            print('Type your new song')
            string = ''
            key = tdl.event.key_wait()
            while key.key != 'ENTER':
                key_char = key.char
                string += key_char
                print(key_char, end='', flush=True)
                key = tdl.event.key_wait()
            tab.set_name(string)
            print()
            return True
        elif choice == 1:  # Existing Tab
            print('Type the filename')
            string = ''
            key = tdl.event.key_wait()
            while key.key != 'ENTER':
                key_char = key.char
                string += key_char
                print(key_char, end='', flush=True)
                key = tdl.event.key_wait()
            print()
            tab.set_name(string)
            return False
        elif choice == 2:  # Existing Tab
            shutil.copy2('./songs/bark_at_the_moon.pyg','./bark_at_the_moon.pyg')
            tab.set_name('bark_at_the_moon')
            return False
        elif choice == 3:  # Existing Tab
            shutil.copy2('./songs/5th-caprice.pyg','./5th-caprice.pyg')
            tab.set_name('5th-caprice')
            return False


###########################################
# Initialization & Main Loop              #
###########################################
is_new_file = main_menu()
filename = tab.get_name()
if is_new_file:
    playhead = Playhead()
else:
    playhead = file_to_fretboard(filename)
    tab.update_bar(playhead, player.x)

user.add_playhead(playhead)
user.add_player(player)


while not tdl.event.is_window_closed():
    # Draw all Objects
    render_all()

    # Scroll Left/Right
    if player.x // SCREEN_WIDTH >= 1 or player.x >= 50:
        scroll_by = (player.x // SCREEN_WIDTH - 1)*SCREEN_WIDTH + player.x % SCREEN_WIDTH + 50
        con.scroll(-scroll_by,0)
        root.blit(con, 0 , 0,  SCREEN_WIDTH  , SCREEN_HEIGHT, 0, 0)
        # Blits con with root at top left of screen
        root.blit(panel, 0, PANEL_Y, SCREEN_WIDTH, PANEL_HEIGHT, 0, 0)
        # Blits con with root at top left of screen
        root.blit(title, 0, 0, SCREEN_WIDTH, 5, 0, 0)

    # Push changes to the Screen
    tdl.flush()

    # Clear all old positions
    player.clear()

    #handle keys and exit game if needed
    exit_game = user.handle_keys()
    if exit_game:
        break

###########################################
# Post-Processing of Music               #
###########################################
def process_notes():
    '''
    Runs through the linked list of playhead nodes from the head to the tail,
    returning a (contigous) list of notes each listing their pitch, start point, and duration.
    '''
    node = playhead.get_head()
    notes = list()
    score_position = 0
    j = 0
    while node != None:
        note = list()
        for i, fret in enumerate(node.notes):
            if fret != None:
                pitch = 40 + ((5 - i) * 5) + int(fret)
                if i < 2:
                    pitch -= 1
                note.append(pitch)
            else:
                note.append(None)
        length = node.get_note_length()
        if j != 0:
            score_position += node.get_previous().get_note_length()
        notes.append((note,length,score_position))
        node = node.get_next()
        j += 1
    for i in notes:
        print(i)
    return notes
def get_note(number):
    notelist = ["c", "cs", "d", "ds", "e", "f", "fs", "g", "gs", "a", "as", "b"]
    return notelist[number]



def guess_key_signature(notes):
    '''
    Takes the notes from a song and uses the melody to make a guess about
    which key signature the song is in.
    '''
    # The highest notes are assumed to contain the melody.
    # Musically, this is a resonable assumption.
    melody = list()
    for details in notes:
        for note in details[0]:
            if note != None:
                melody.append(get_note(note%12))
                break

    # Creates a set of notes in the song
    note_frequency = Counter(melody) # May note need this
    note_set = set()
    # If there are more than 7 pitches in the melody,
    # use only the 7 most frequent to guess key signature.
    n = len(note_frequency)
    for i,note in enumerate(note_frequency.most_common(n)):
        if i >= 7:
            break
        note_set.add(note[0])

    # Create a dictionary of key signatures based of
    # the pattern of the diatonic scale (i.e. doh-ray-me-...)
    diatonic_pattern = [0,2,4,5,7,9,11]
    key_signatures = dict()
    for i in range(12):
        sig = set()
        for note in diatonic_pattern:
            sig.add(get_note((note + i)%12))
        key_signatures[str(get_note(i))] = sig

    # Index the intersection of the notes from the melody
    # and the various keys of the major scale.
    # The largest intersection is the closest match.
    closest_val = 0
    closest_key = None
    for key,value in key_signatures.items():
        intersections = len(note_set.intersection(value))
        if intersections > closest_val:
            closest_val = intersections
            closest_key = key

    key_for_song = closest_key + ' \major'
    return key_for_song


notes = process_notes()
key_for_song = guess_key_signature(notes)
mus.write_midi(filename + '.mid', notes,tempo = playhead.get_tempo())
createsheet(filename +'.ly', notes, key=key_for_song)
mus.play_midi(filename + '.mid')
