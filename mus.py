from Libraries.MIDIUtil.MidiFile3 import MIDIFile as midi
import pygame

# import numpy as np

"""
Dependencies:
    MIDIUtil   - https://code.google.com/archive/p/midiutil/
    TiMidity++ - http://timidity.sourceforge.net/
    pygame
    yum -> repo required, place in /etc/yum/yum.conf (needs root access):
        [linuxtech]
        name=LinuxTECH
        baseurl=http://pkgrepo.linuxtech.net/el6/release/
        enabled=1
        gpgcheck=1
        gpgkey=http://pkgrepo.linuxtech.net/el6/release/RPM-GPG-KEY-LinuxTECH.NET

        then:
        yum install fluid-soundfont-lite-patches

Notes:
    MIDIUtil - used to create and then save midi file
"""


def write_midi(music_file, noteArray, tempo=120):
    """
    Reference:
        MyMIDI.addNote(track,channel,pitch,time,duration,volume)
    """

    # MIDIUtil initialization
    my_midi = midi(6) # 6 tracks for 6 strings
    channel = 0
    volume = 100

    for i in range(6):  # intitializes all tracks
        # my_midi.addTrackName(track, time, "Sample Track")
        my_midi.addTempo(i, 0, tempo)  # last int is beats per minute
        # changes instrument to acoustic guitar (25-30 are midi guitars)
        my_midi.addProgramChange(i, 0, 0, 27)  # last num is instrument

    # Adds notes to file
    size = len(noteArray)  # gets columns of numpy array (length of song)
    for i in range(size):
        # gets note length from bottom of array
        dur = noteArray[i][1]
        for j in range(6): # for each string, add note at beat to track
            try:
                pitch = noteArray[i][0][j]
            except IndexError:
                pitch = None
            if pitch is not None:
                my_midi.addNote(j, channel, pitch, noteArray[i][2], dur, volume)

    # writes the midi
    with open(music_file, 'wb') as f:
        my_midi.writeFile(f)


############################################################################
# Retireved from https://gist.github.com/naotokui/29073690279056e9354e6259efbf8f30
# -slight modification: default player settings in play_midi()
# -added print statement for better user comprehension
############################################################################
def play_music(music_file):
    """
    stream music with mixer.music module in blocking manner
    this will stream the sound from disk while playing
    """
    clock = pygame.time.Clock()
    try:
        pygame.mixer.music.load(music_file)
        print("Music file %s loaded!" % music_file)
    except pygame.error:
        print("File %s not found! (%s)" % (music_file, pygame.get_error()))
        return
    print("Playing...") # added
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy():
        # check if playback has finished
        clock.tick(30)


def play_midi(music_file):
    # was not a funciton, but changed for our use
    # pick a midi music file you have ...
    # (if not in working folder use full path)

    freq = 44100    # audio CD quality
    bitsize = -16   # unsigned 16 bit
    channels = 2    # 1 is mono, 2 is stereo
    buffer = 1024    # number of samples
    pygame.mixer.init(freq, bitsize, channels, buffer)

    # optional volume 0 to 1.0
    pygame.mixer.music.set_volume(0.8)
    try:
        play_music(music_file)
    except KeyboardInterrupt:
        # if user hits Ctrl/C then exit
        # (works only in console mode)
        pygame.mixer.music.fadeout(1000)
        pygame.mixer.music.stop()
        raise SystemExit
############################################################################

# def make_sheet(filename_in, filename_out):
#     composition = m.midi.midi_file_in.MIDI_to_Composition(filename_in)
#     lily_str = m.extra.lilpond.from_Composition(composition)
#     m.extra.lilpond.to_pdf(lily_str, filename_out)

# fname = "output_test.mid"  # File name to be used
#
# # MIDIUtil initialization
# my_midi = midi(6)
# track = 0
# channel = 0
# time = 0
# duration = 1
# volume = 100
#
# my_midi.addTrackName(track, time, "Sample Track")
# my_midi.addTempo(track, time, 120)
# # changes instrument to acustic guitar
# my_midi.addProgramChange(track, channel, time, 25)
#
# # adds notes to file TODO: algorithm to convert to notes
# my_midi.addNote(track, channel, 60, time, 0.5, volume)
# my_midi.addNote(track, channel, 90, 1, 2, volume)
#
# # writes the midi
# with open(fname, 'wb') as f:
#     my_midi.writeFile(f)
#
# play_midi(output_test.mid)
