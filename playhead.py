class Playhead_Node:
    '''
    Each node represents a point the playhead will rearch in the song.
    While each note does occur sequentially, composing music often requires
    inserting and deleting notes at many points in a composition.

    Thus, a linked list of nodes, eaching containing features of a point
    in a song, such as notes played and their length, is ideal.
    A python list would require O(n) for each addition/insertion,
    but a linked list is O(1), where n is the number of notes in the list.

    '''
    def __init__(self, note_length):
        # List of notes on each string where 0 = 1st string (top/highest)
        # and 5 = 6th string(bottom/lowest)
        self.notes = [None for x in range(6)]
        # Note is an object, node is a playhead node
        self.note_length = note_length  # defaults to 1/4 note
        self.next = None
        self.previous = None

    def get_notes(self):
        return self.notes

    def get_next(self):
        return self.next

    def get_previous(self):
        return self.previous

    def set_note(self, string, fret):
        self.notes[string] = fret

    def get_note_length(self):
        return self.note_length

    def set_note_length(self, length):
        self.note_length = length

    def set_next(self, new_next):
        self.next = new_next

    def set_previous(self, new_previous):
        self.previous = new_previous


class Playhead:
    '''
    The playhead is the data structure for the entire score. It is how the user interacts with the data
    and how changes are communicated to the tab module.

    The playhead points to a node in a linked list which represents a particular point in the song. The playhead
    can be thought of as a line below the current note the user is working with.

    There are different potential ways of dealing with incomplete bars such as preventing the user
    from leaving incomplete/overfilled bars until fixed, ignoring bar lengths altogether,
    or dynamically allocated bars.

    The modular structure of the program makes dynamic allocation of bar lengths ideal, as no
    changes to the data structure are required and updating visual represetnations of the music
    takes constant time (See update_bar() in tab.py for more details).

    NOTE: Non-integer bar lengths will cause some visual errors (i.e. if bar lengths add up to 3.5 o 4.25).
    This is easily fixed by adding a note of correct length or updating the length of the current note.

    Note that nodes in the linked list are not actually notes. The playhead nodes can contain multiple
    notes and also other features.
    '''
    # for playhead at note k, score position is:
    # p(k) = sum(i->k-1)[i.note_length]
    def __init__(self):
        # Never an empty tab
        self.head = Playhead_Node(1)
        self.tail = self.head
        self.current_node = self.head
        self.score_position = 0  # Int value indicating which note
        self.length = 1
        self.string = 0  # String the cursor is on
        self.playhead_length = 1
        self.tempo = 120

    def get_tempo(self):
        return self.tempo

    def change_tempo(self, change):
        self.tempo += change

    def get_node(self):
        return self.current_node

    def set_node(self, node):
        self.current_node = node

    def get_length(self):
        return self.length

    def get_head(self):
        return self.head

    def set_head(self, node):
        self.head = node

    def set_tail(self, node):
        self.tail = node

    def get_score_position(self):
        return self.score_position

    def move_right(self):
        next_node = self.current_node.get_next()
        current_note_length = self.current_node.get_note_length()

        # Continue the score if at the end
        self.score_position += current_note_length
        if next_node is None:
            new_node = Playhead_Node(self.playhead_length)
            self.insert_node(new_node)
            next_node = self.current_node.get_next()

        self.current_node = next_node

    def move_left(self):
        previous_node = self.current_node.get_previous()

        # Can't move left of the first node
        if previous_node is None:
            print('non')
            return

        # Move the playhead to the previous node
        self.score_position -= previous_node.get_note_length()
        self.current_node = previous_node

    def insert_node(self, new_node):
        node = self.get_node()

        if node.get_next() is None:
            self.tail = new_node
            new_node.set_next(None)
        else:
            new_node.set_next(node.get_next())

        node.set_next(new_node)  # Must be after if statement or tail doesn't update
        new_node.set_previous(node)

        self.length += 1

    def delete_node(self):
        node = self.current_node

        # Cant delete first node
        if node.previous is None:
            return

        # Link 2nd last to None
        if node.get_next() is None:
            node.get_previous().set_next(None)
        else:
            next = node.get_next()
            previous = node.get_previous()
            next.set_previous(previous)
            previous.set_next(next)

        self.score_position -= node.get_previous().get_note_length()
        self.current_node = node.get_previous()
        self.length -= 1

    # Update NOTE not NODE!
    def update_note(self, fret):
        self.get_node().set_note(self.string, fret)

    def update_note_length(self, length):
        self.get_node().set_note_length(length)
        self.playhead_length = length

    def get_string(self):
        return self.string

    def set_string(self, new_string):
        self.string = new_string

    def get_bar_length(self):
        return self.bar_length

