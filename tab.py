class Fret:
    # a tile of the map and its properties
    def __init__(self, char = ' '):
        self.char = char


class Tab:
    '''
    Tab is an object wrapper for graphically representing the score.
    The main purpose is to interface with the User module,
    Read data from the Playhead module, and display it to the screen.
    '''
    def __init__(self):
        # Tab and TDL settings
        self.width = 5000  # Max Width of Tab
        self.height = 8  # Number of string + 2 rows for rhythm
        self.background_color = (0, 0, 0)
        self.char_color = (255, 255, 255)
        self.limit_fps = 40
        self.name = ''  # Name of the Song used in filenames and display in GUI
        # Fretboard is a 2x2 matrix (list of lists),
        # to display all graphical details for the tab
        self.fretboard = [[Fret() for y in range(self.height)] for y in range(self.width)]
        self.init_fretboard()

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def get_height(self):
        return self.height

    def get_width(self):
        return self.width

    def get_fretboard(self):
        return self.fretboard

    # Adds a vertical line of a particular character
    def add_char_line(self, column, char):
        for i in range(6):
            self.fretboard[column][i].char = char

    # Creates the beginning of the score
    def init_fretboard(self):
        # Map[col][row]. tab.width = # columns; tab.height = # rows
        # First Bar Line
        self.add_char_line(0, '|')
        self.add_char_line(1, '|')
        self.add_char_line(2, '-')
        self.add_char_line(3, '-')
        self.add_char_line(4, '-')
        self.add_char_line(5, '|')
        self.add_char_line(6, '|')
        self.fretboard[2][6].char = '|'
        self.fretboard[2][7].char = '|'

    def update_bar(self, playhead, cursor_x):
        '''
        Gets the current node they playhead is point to,
        and from the points updates the tab from that node foward.
        For large score, this function is the major bottleneck, with a loop running O(n) times,
        where O(n) is, at most, the width of the score.

        At this bottleneck, having separate modules for data storage and data representation is helpful.
        Since the tab module makes no changes to any data in the playhead module, the main loop of the
        update_bar functions only needs to run enough times to make visible changes (100 is sufficient).
        By modularizing the architecture, the complexity of the function is reduced from O(n) to O(1).
        '''
        node = playhead.get_node()
        score_position = playhead.get_score_position()
        previous = (score_position - node.get_note_length()) // 4
        # Prevents Moving offscreen to the left
        if previous < 0:
            previous = 0
        off_screen = 0  # limts the loop to 100 runs
        while node is not None and off_screen < 100:
            current_bar = score_position // 4
            # Adds the extra dashes and bar staffs when the bar has 4/4 beats.
            if current_bar != previous:
                # Bar line
                self.add_char_line(cursor_x + 3, '|')
                cursor_x += 1

            # Be careful to remember the playhead is only moved by the cursor,
            # not by any function calls in the tab module.
            self.add_staff(playhead, node, score_position, cursor_x)
            self.add_char_line(cursor_x + 3, '-')
            self.add_char_line(cursor_x + 4, '-')
            self.add_char_line(cursor_x + 5, '-')

            # Adds any notes for the current column
            notes = node.get_notes()
            for i, note in enumerate(notes):
                if note is not None:
                    self.enter_note(str(note), cursor_x + 3, i)

            cursor_x += 3
            score_position += node.get_note_length()  # Add before updating node
            previous = current_bar  # Updated for next loop iteration to check when bar changes
            node = node.get_next()  # Move to the next node
            off_screen += 1
        # Add the final 2 bar staffs
        self.add_char_line(cursor_x + 3, '|')
        self.add_char_line(cursor_x + 4, '|')

    def add_staff(self, layhead, node, pos, cursor_x):
        # Helper function that adds the rhythm staffs below the notes
        staff_at = cursor_x + 3
        self.fretboard[staff_at][6].char = '|'
        self.fretboard[staff_at][7].char = '|'

        self.fretboard[staff_at - 1][6].char = ' '
        self.fretboard[staff_at - 1][7].char = ' '
        self.fretboard[staff_at + 1][6].char = ' '
        self.fretboard[staff_at + 1][7].char = ' '
        self.fretboard[staff_at + 2][6].char = ' '
        self.fretboard[staff_at + 2][7].char = ' '
        self.fretboard[staff_at + 3][6].char = ' '
        self.fretboard[staff_at + 3][7].char = ' '
        nl = node.get_note_length()

        # Different staff arrangments required for different note lengths
        if nl == 1:
            pass
        elif nl == 0.5:
            if pos % 1 == 0:
                self.fretboard[staff_at + 1][7].char = '_'
            else:
                self.fretboard[staff_at - 1][7].char = '_'
        elif nl == 0.25:
            if pos % 1 == 0:
                self.fretboard[staff_at + 1][6].char = '_'
                self.fretboard[staff_at + 1][7].char = '_'
            elif pos % 1 == 0.75:
                self.fretboard[staff_at - 1][6].char = '_'
                self.fretboard[staff_at - 1][7].char = '_'
            else:
                self.fretboard[staff_at - 1][6].char = '_'
                self.fretboard[staff_at - 1][7].char = '_'
                self.fretboard[staff_at + 1][6].char = '_'
                self.fretboard[staff_at + 1][7].char = '_'

    def enter_note(self, note, cursor_x, cursor_y):
        # Add staff at leftmost fret number and update length of song in beats
        self.fretboard[cursor_x][cursor_y].char = note[0]
        cursor_x += 1
        try:
            self.fretboard[cursor_x][cursor_y].char = note[1]
        except:
            self.fretboard[cursor_x][cursor_y].char = '-'

